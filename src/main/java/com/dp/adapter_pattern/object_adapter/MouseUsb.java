package com.dp.adapter_pattern.object_adapter;

/**
 * 鼠标实现USB类
 */
class MouseUsb implements Usb {

    public void use() {
        System.out.println("我是鼠标，可以使用USB接口");
    }
}
