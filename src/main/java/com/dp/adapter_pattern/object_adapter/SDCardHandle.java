package com.dp.adapter_pattern.object_adapter;

/**
 * SDCard 类
 */
class SDCardHandle implements SDCard {
    public void useSD() {
        System.out.println("我是SD卡，经过转换才能使用USB接口");
    }
}
