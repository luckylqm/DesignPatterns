package com.dp.adapter_pattern.object_adapter;

/**
 * @author ：廖权名 qq:1528490339
 * @description ：适配器模式(对象适配器模式)
 * 通过对象实现
 * @date ：Created in 2019/3/4 1:00
 */


/**
 * 主类
 */
public class ObjectAdapterTest {
    public static void main(String[] args) {
        Usb usb1 = new MouseUsb();
        usb1.use();
        Usb usb2=new SDCardAdapter(new SDCardHandle());
        usb2.use();

    }
}