package com.dp.adapter_pattern.object_adapter;

/**
 * SDCard接口
 */
interface SDCard{
    void useSD();
}
