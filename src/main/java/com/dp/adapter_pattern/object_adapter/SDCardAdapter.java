package com.dp.adapter_pattern.object_adapter;

/**
 * SDCard适配器，也就是现实中的读卡器
 */
class SDCardAdapter implements Usb {
    SDCardHandle sdCardHandle;

    public SDCardAdapter(SDCardHandle sdCardHandle) {
        this.sdCardHandle = sdCardHandle;
    }

    public void use() {
        sdCardHandle.useSD();
    }
}
