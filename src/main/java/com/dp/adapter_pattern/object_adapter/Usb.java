package com.dp.adapter_pattern.object_adapter;

/**
 * USB接口
 */
interface Usb {
    void use();
}
