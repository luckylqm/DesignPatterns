package com.dp.adapter_pattern.class_adapter;

/**
 * @author ：廖权名 qq:1528490339
 * @description ：适配器模式(类适配器模式)
 * 通过类继承的方式实现
 * @date ：Created in 2019/3/4 1:20
 */
public class ClassAdapterTest {
    public static void main(String[] args) {
        Usb usb1 = new MouseUsb();
        usb1.use();
        Usb usb2 = new SDCardAdapter();
        usb2.use();
    }
}


