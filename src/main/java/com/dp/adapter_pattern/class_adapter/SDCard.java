package com.dp.adapter_pattern.class_adapter;

/**
 * SDCard接口
 */
interface SDCard {
    void useSD();
}
