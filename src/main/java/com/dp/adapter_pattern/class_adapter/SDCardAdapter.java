package com.dp.adapter_pattern.class_adapter;

/**
 * SDCard适配器，也就是现实中的读卡器,继承了SDCard
 */
class SDCardAdapter extends SDCardHandle implements Usb {

    public void use() {
        super.useSD();
    }
}
