package com.dp.adapter_pattern.class_adapter;

/**
 * USB接口
 */
interface Usb {
    void use();
}
